import sys as s
import xml.etree.ElementTree as xml
import psycopg2 as psc


def compare_tag(m, part):  # Функция сравнения
    list_compare = ['flag'] * len(part)   
    list_content = ['null'] * len(m)   # Лист соответсвия для мастера
    outdata = []    # Лист выхода
    for k in range(len(part)):
        for j in range(len(m)):
            if (part[k].attrib == m[j].attrib) and (part[k].tag == m[j].tag):  # Сравнение трибутов таблиц
                list_compare[k] = [part[k]]
                list_content[j] = ['not null']  
                outdata.append([j, k])     
                # Или сравнение , как то обыграть надо этот момент
                break
    # list_part = []
    try:
        k = list_compare.index('flag')  # Поиск элемента некорректного эллемента
        list_compare[k] = ['null']
        print('Шаблоны не совпадают в секции', m.tag)
        print("\t", ' В реплике', part[k].tag,  'с параметрами', part[k].attrib)
        raise SystemExit
    except ValueError:
        k = -1

    try:
        j = list_content.index('null')
        list_content[j] = ['correct']
        print('Шаблоны не совпадают в секции', m.tag)
        print("\t", ' В реплике отсутствует', m[j].tag, 'с параметрами', m[j].attrib)
        raise SystemExit
    except ValueError:
        j = -1
    return outdata


def compare_children(mch, partch, zone):
    outdata = []
    list_compare = ['elem'] * len(partch)  # Определение списка соответствия для  детей в малом файле
    list_content = ['null'] * len(mch)
    for a in range(len(partch)):
        for b in range(len(mch)):  # Пробег по тегам общего файла
            if (partch[a].attrib == mch[b].attrib) and (partch[a].tag == mch[b].tag):
                # заполнение списков при наличии соответствия
                list_compare[a] = partch[a]
                list_content[b] = 'not null'
                outdata.append([b, a])
                break
    try:
        a = list_compare.index('elem')
        list_compare[a] = ['null']
        print('Шаблоны не совпадают в секции', zone)
        print("\t", ' В реплике  в', partch.tag, 'с параметрами', partch.attrib, ':',
              partch[a].tag,  'с параметрами', partch[a].attrib)
        raise SystemExit
    except ValueError:
        a = -1

    try:
        b = list_content.index('null')
        list_content[b] = 'correct'
        print('Шаблоны не совпадают в секции', zone)
        print("\t", ' В реплике  в ', mch.tag, 'с параметрами', mch.attrib, 'отсутствует',
              mch[b].tag, 'с параметрами', mch[b].attrib)
        raise SystemExit
    except ValueError:
        b = -1
    return outdata


def connect_sys(input):
    # ### CONNECT ##################

    if len(input) < 9:
        print('Слишком мало параметров. Попробуйте еще раз!\n',
              'На вход должно поступать 8 параметров: Две пары шаблонов( Файлы должны иметь расширение *.xml),',
              'а так же названия двух БД и  их портов , которым они соответсвуют\n',
              'Например: example1.xml example2.xml example3.xml example4.xml '
              'exapmpleport1 exampleDB1 exapmpleport1 exampleDB2')
        raise SystemExit
    try:
        com1 = xml.parse(input[1]).getroot()
    except FileNotFoundError:
        print('Файла', input[1],
              ' не существует. Попробуйте еще раз! \n Проверьте корректность названия.'
              ' Файл должен иметь расширение *.xml')
        raise SystemExit
    try:
        part1 = xml.parse(input[2]).getroot()
    except FileNotFoundError:
        print('Файла', input[2],
              ' не существует. Попробуйте еще раз! \n Проверьте корректность названия. '
              'Файл должен иметь расширение *.xml')
        raise SystemExit
    try:
        com2 = xml.parse(input[3]).getroot()
    except FileNotFoundError:
        print('Файла', input[3],
              ' не существует. Попробуйте еще раз! \n Проверьте корректность названия. '
              'Файл должен иметь расширение *.xml')
        raise SystemExit
    try:
        part2 = xml.parse(input[4]).getroot()
    except FileNotFoundError:
        print('Файла', input[4],
              'не существует. Попробуйте еще раз! \n '
              'Проверьте корректность названия. Файл должен иметь расширение *.xml')
        raise SystemExit
    # c = psc.connect(dbname="test11", user="postgres", password="1234567", host="localhost", port="5432")
    try:
        pass
        c_1 = psc.connect(dbname=input[6], user="postgres", password="1234567", host="localhost", port=input[5])
    except psc.OperationalError:
        print('Базы с названием', input[6],
              'не существует. Либо порт', input[5], 'указан неверно. \n',
              ' Попробуйте еще раз! \n Проверьте корректность написания аргумента.')
        # print('Базы с таким названием не существует. Попробуйте еще раз! \n
        # Проверьте корректность написания аргумента.')
        raise SystemExit
    try:
        pass
        c_2 = psc.connect(dbname=input[8], user="postgres", password="1234567", host="localhost", port=input[7])
    except psc.OperationalError:
        print('Базы с названием', input[8],
              'не существует. Либо порт', input[7], 'указан неверно. \n' 
              ' Попробуйте еще раз! \n Проверьте корректность написания аргумента.')
        # print('Базы с таким названием не существует. Попробуйте еще раз! \n
        # Проверьте корректность написания аргумента.')
        raise SystemExit
    return [com1, part1, com2, part2, c_1, c_2]
    # # END CONNECT ##############


connect_out = connect_sys(s.argv)
pub = "publication"
sub = "subscription"
common1 = connect_out[0]
xml_part1 = connect_out[1]
common2 = connect_out[2]
xml_part2 = connect_out[3]
c1 = connect_out[4]
c2 = connect_out[5]
# # BODY ###########
tag_flag = False
for element in common1.iter(pub):  # Поиск publication.
    tag_flag = True
    # print( 'i was born')
    mainp = element
    temp_public = compare_tag(mainp, xml_part1)           # Сравнение тэгов
    for i in range(len(temp_public)):
        # Сравнение детей
        temp_public_ch = compare_children(mainp[temp_public[i][0]], xml_part1[temp_public[i][1]], pub)
        #temp_public_ch = compare_tag(mainp[temp_public[i][0]], xml_part1[temp_public[i][1]], pub)
        # print(temp_public_ch)

# TEST OF ZONE ##########
if not tag_flag:
    print('В файлe', s.argv[1],
          'нет области publication \n Возможно, это не тот файл? Проверьте еще раз!')
    raise SystemExit

tag_flag = False
for element in common2.iter(sub):
    tag_flag = True
    mains = element
    temp_sub = compare_tag(mains, xml_part2)  # Сравнение тэгов
    temp_sub_ch = []
    for i in range(len(temp_sub)):
        # Сравнение детей
        # print(temp_sub[i][0])
        temp_sub_ch.append(compare_children(mains[temp_sub[i][0]], xml_part2[temp_sub[i][1]], sub))
        # temp_sub_ch = compare_tag(mains[temp_sub[i][0]], xml_part2[temp_sub[i][1]])
temp_sub_ch = temp_sub_ch[0]
# TEST OF ZONE ##########
if not tag_flag:
    print('В файлe', s.argv[3],
          'нет области subscription \n Возможно, это не тот файл? Проверьте еще раз!')
    raise SystemExit

print('\n Шаблоны совпадают! \n')
# BASE #############

cur1 = c1.cursor()
cur2 = c2.cursor()

# raise SystemExit
for i in range(len(temp_sub)  ):
    name = mains[temp_sub[i][0]].attrib.get('name')
    col_list = []
    # print(mains[temp_sub[0][0]][temp_sub_ch[0][0]].attrib)
    for j in range(len(mains[temp_sub[i][0]])):
        if mains[temp_sub[i][0]][temp_sub_ch[j][0]].attrib.get('uid') != None:
            name_key = mains[temp_sub[i][0]][temp_sub_ch[j][0]].attrib.get('name')
        else:
            col_list.append(mains[temp_sub[i][0]][temp_sub_ch[j][0]].attrib.get('name'))
    str_select = ""
    for l in range(len(col_list) - 1):
        str_select = str_select + col_list[l] + ","
    str_select = str_select + col_list[len(col_list) -1]
    str_select_ = "SELECT " + str_select
    # print(str_select_)
    try:

        try:
            cur1.execute(str_select_ + " FROM " + name + " where " + name_key + "=1")

        except psc.errors.UndefinedColumn:
            print('Название одной или нескольких колонок', str_select,
                  ' не совпадает с шаблоном')
            raise SystemExit
    except psc.errors.UndefinedTable:
        print('Название таблицы', name,
              ' не совпадает с шаблоном')
        raise SystemExit
    try:
        try:
            cur2.execute(str_select_ + " FROM " + name + " where " + name_key + "=1")
            # Нужно доработать для многих строк, не только первой
        except psc.errors.UndefinedColumn:
            print('Название одной или нескольких колонок', str_select,
                  ' не совпадает с шаблоном и другой базой')
            raise SystemExit
    except psc.errors.UndefinedTable:
        print('Название таблицы', name,
              ' не совпадает с шаблоном и другой базой')
        raise SystemExit

    temp1 = cur1.fetchall()
    temp2 = cur2.fetchall()
    for k in range(len(temp1)):
        
        if temp1[k] != temp2[k]:
            print('Содержимое таблиц не совпадает')
            raise SystemExit

print('\n Репликация  сработала')

cur1.close()
cur2.close()
c1.close()
c2.close()






